//Maya ASCII 2018 scene
requires maya "2018";
currentUnit -l meter -a degree -t film;
createNode script -n "upAxisScriptNode";
	setAttr ".b" -type "string" "string $currentAxis = `upAxis -q -ax`; if ($currentAxis != \"y\") { upAxis -ax \"y\"; viewSet -home persp; }";
	setAttr ".st" 2;
createNode transform -n "pPlane1";
	addAttr -ln "colladaId" -dt "string";
	setAttr .colladaId -type "string" "pPlane1";
	setAttr ".t" -type "double3" 0.000000 1.500000 0.000000;
	setAttr ".r" -type "double3" 90.000000 0.000000 0.000000;
	setAttr ".s" -type "double3" 4.000000 1.000000 3.000000;
createNode transform -n "pCube1";
	addAttr -ln "colladaId" -dt "string";
	setAttr .colladaId -type "string" "pCube1";
	setAttr ".t" -type "double3" -1.512326 2.147774 -0.025000;
	setAttr ".s" -type "double3" 0.602819 0.789012 0.050000;
createNode transform -n "pCube2";
	addAttr -ln "colladaId" -dt "string";
	setAttr .colladaId -type "string" "pCube2";
	setAttr ".t" -type "double3" -0.506341 2.147774 -0.025000;
	setAttr ".s" -type "double3" 0.602819 0.789012 0.050000;
createNode transform -n "pCube3";
	addAttr -ln "colladaId" -dt "string";
	setAttr .colladaId -type "string" "pCube3";
	setAttr ".t" -type "double3" 0.489045 2.147774 -0.025000;
	setAttr ".s" -type "double3" 0.602819 0.789012 0.050000;
createNode transform -n "pCube4";
	addAttr -ln "colladaId" -dt "string";
	setAttr .colladaId -type "string" "pCube4";
	setAttr ".t" -type "double3" 1.489228 2.147774 -0.025000;
	setAttr ".s" -type "double3" 0.602819 0.789012 0.050000;
createNode transform -n "pCube5";
	addAttr -ln "colladaId" -dt "string";
	setAttr .colladaId -type "string" "pCube5";
	setAttr ".t" -type "double3" 1.489228 1.117396 -0.025000;
	setAttr ".s" -type "double3" 0.602819 0.789012 0.050000;
createNode transform -n "pCube6";
	addAttr -ln "colladaId" -dt "string";
	setAttr .colladaId -type "string" "pCube6";
	setAttr ".t" -type "double3" 0.489045 1.117396 -0.025000;
	setAttr ".s" -type "double3" 0.602819 0.789012 0.050000;
createNode transform -n "pCube7";
	addAttr -ln "colladaId" -dt "string";
	setAttr .colladaId -type "string" "pCube7";
	setAttr ".t" -type "double3" -0.506341 1.117396 -0.025000;
	setAttr ".s" -type "double3" 0.602819 0.789012 0.050000;
createNode transform -n "pCube8";
	addAttr -ln "colladaId" -dt "string";
	setAttr .colladaId -type "string" "pCube8";
	setAttr ".t" -type "double3" -1.512326 1.117396 -0.025000;
	setAttr ".s" -type "double3" 0.602819 0.789012 0.050000;
createNode transform -n "polySurface1";
	addAttr -ln "colladaId" -dt "string";
	setAttr .colladaId -type "string" "polySurface1";
createNode transform -n "polySurface2";
	addAttr -ln "colladaId" -dt "string";
	setAttr .colladaId -type "string" "polySurface2";
createNode transform -n "polySurface3";
	addAttr -ln "colladaId" -dt "string";
	setAttr .colladaId -type "string" "polySurface3";
createNode transform -n "polySurface4";
	addAttr -ln "colladaId" -dt "string";
	setAttr .colladaId -type "string" "polySurface4";
createNode transform -n "polySurface5";
	addAttr -ln "colladaId" -dt "string";
	setAttr .colladaId -type "string" "polySurface5";
createNode transform -n "polySurface6";
	addAttr -ln "colladaId" -dt "string";
	setAttr .colladaId -type "string" "polySurface6";
createNode transform -n "polySurface7";
	addAttr -ln "colladaId" -dt "string";
	setAttr .colladaId -type "string" "polySurface7";
createNode transform -n "polySurface8";
	addAttr -ln "colladaId" -dt "string";
	setAttr .colladaId -type "string" "polySurface8";
	setAttr ".t" -type "double3" 1.000000 0.000000 0.100000;
createNode transform -n "pPlane2";
	addAttr -ln "colladaId" -dt "string";
	setAttr .colladaId -type "string" "pPlane2";
	setAttr ".t" -type "double3" 1.000000 1.800000 0.000000;
	setAttr ".s" -type "double3" 4.000000 1.000000 0.200000;
createNode transform -n "pPlane3";
	addAttr -ln "colladaId" -dt "string";
	setAttr .colladaId -type "string" "pPlane3";
	setAttr ".t" -type "double3" 0.000000 2.000000 0.000000;
	setAttr ".r" -type "double3" 90.000000 0.000000 0.000000;
	setAttr ".s" -type "double3" 4.000000 1.000000 4.000000;
createNode transform -n "pCube9";
	addAttr -ln "colladaId" -dt "string";
	setAttr .colladaId -type "string" "pCube9";
	setAttr ".t" -type "double3" -1.018835 2.762653 0.800000;
	setAttr ".s" -type "double3" 1.143738 1.438058 2.000000;
createNode transform -n "pCube10";
	addAttr -ln "colladaId" -dt "string";
	setAttr .colladaId -type "string" "pCube10";
	setAttr ".t" -type "double3" 0.993111 2.762653 0.800000;
	setAttr ".s" -type "double3" 1.143738 1.438058 2.000000;
createNode transform -n "polySurface9";
	addAttr -ln "colladaId" -dt "string";
	setAttr .colladaId -type "string" "polySurface9";
createNode transform -n "polySurface10";
	addAttr -ln "colladaId" -dt "string";
	setAttr .colladaId -type "string" "polySurface10";
	setAttr ".t" -type "double3" 1.000000 0.000000 0.000000;
createNode transform -n "polySurface11";
	addAttr -ln "colladaId" -dt "string";
	setAttr .colladaId -type "string" "polySurface11";
createNode transform -n "polySurface12" -p "|polySurface11";
	addAttr -ln "colladaId" -dt "string";
	setAttr .colladaId -type "string" "polySurface12";
createNode transform -n "polySurface13" -p "|polySurface11";
	addAttr -ln "colladaId" -dt "string";
	setAttr .colladaId -type "string" "polySurface13";
createNode transform -n "polySurface14" -p "|polySurface11";
	addAttr -ln "colladaId" -dt "string";
	setAttr .colladaId -type "string" "polySurface14";
createNode transform -n "polySurface15";
	addAttr -ln "colladaId" -dt "string";
	setAttr .colladaId -type "string" "polySurface15";
createNode transform -n "polySurface16" -p "|polySurface15";
	addAttr -ln "colladaId" -dt "string";
	setAttr .colladaId -type "string" "polySurface16";
createNode transform -n "polySurface17" -p "|polySurface15";
	addAttr -ln "colladaId" -dt "string";
	setAttr .colladaId -type "string" "polySurface17";
createNode transform -n "polySurface18" -p "|polySurface15";
	addAttr -ln "colladaId" -dt "string";
	setAttr .colladaId -type "string" "polySurface18";
createNode blinn -n "blinn1";
	setAttr ".dc" 1.000000;
	setAttr ".ec" 0.300000;
	addAttr -ln "colladaEffectId" -dt "string";
	setAttr .colladaEffectId -type "string" "blinn1-fx";
	addAttr -ln "colladaMaterialId" -dt "string";
	setAttr .colladaMaterialId -type "string" "blinn1";
createNode shadingEngine -n "blinn1SG";
createNode materialInfo -n "materialInfo_1";
createNode place2dTexture -n "place2dTexture_1";
createNode place2dTexture -n "place2dTexture_2";
createNode blinn -n "blinn2";
	setAttr ".dc" 1.000000;
	setAttr ".ec" 0.300000;
	addAttr -ln "colladaEffectId" -dt "string";
	setAttr .colladaEffectId -type "string" "blinn2-fx";
	addAttr -ln "colladaMaterialId" -dt "string";
	setAttr .colladaMaterialId -type "string" "blinn2";
createNode shadingEngine -n "blinn2SG";
createNode materialInfo -n "materialInfo_2";
createNode place2dTexture -n "place2dTexture_3";
createNode place2dTexture -n "place2dTexture_4";
createNode blinn -n "blinn3";
	setAttr ".dc" 1.000000;
	setAttr ".ec" 0.300000;
	addAttr -ln "colladaEffectId" -dt "string";
	setAttr .colladaEffectId -type "string" "blinn3-fx";
	addAttr -ln "colladaMaterialId" -dt "string";
	setAttr .colladaMaterialId -type "string" "blinn3";
createNode shadingEngine -n "blinn3SG";
createNode materialInfo -n "materialInfo_3";
createNode place2dTexture -n "place2dTexture_5";
createNode place2dTexture -n "place2dTexture_6";
createNode blinn -n "blinn4";
	setAttr ".dc" 1.000000;
	setAttr ".ec" 0.300000;
	addAttr -ln "colladaEffectId" -dt "string";
	setAttr .colladaEffectId -type "string" "blinn4-fx";
	addAttr -ln "colladaMaterialId" -dt "string";
	setAttr .colladaMaterialId -type "string" "blinn4";
createNode shadingEngine -n "blinn4SG";
createNode materialInfo -n "materialInfo_4";
createNode place2dTexture -n "place2dTexture_7";
createNode place2dTexture -n "place2dTexture_8";
createNode blinn -n "blinn5";
	setAttr ".dc" 1.000000;
	setAttr ".ec" 0.300000;
	addAttr -ln "colladaEffectId" -dt "string";
	setAttr .colladaEffectId -type "string" "blinn5-fx";
	addAttr -ln "colladaMaterialId" -dt "string";
	setAttr .colladaMaterialId -type "string" "blinn5";
createNode shadingEngine -n "blinn5SG";
createNode materialInfo -n "materialInfo_5";
createNode place2dTexture -n "place2dTexture_9";
createNode place2dTexture -n "place2dTexture_10";
createNode blinn -n "blinn6";
	setAttr ".dc" 1.000000;
	setAttr ".ec" 0.300000;
	addAttr -ln "colladaEffectId" -dt "string";
	setAttr .colladaEffectId -type "string" "blinn6-fx";
	addAttr -ln "colladaMaterialId" -dt "string";
	setAttr .colladaMaterialId -type "string" "blinn6";
createNode shadingEngine -n "blinn6SG";
createNode materialInfo -n "materialInfo_6";
createNode place2dTexture -n "place2dTexture_11";
createNode place2dTexture -n "place2dTexture_12";
createNode blinn -n "blinn7";
	setAttr ".dc" 1.000000;
	setAttr ".ec" 0.300000;
	addAttr -ln "colladaEffectId" -dt "string";
	setAttr .colladaEffectId -type "string" "blinn7-fx";
	addAttr -ln "colladaMaterialId" -dt "string";
	setAttr .colladaMaterialId -type "string" "blinn7";
createNode shadingEngine -n "blinn7SG";
createNode materialInfo -n "materialInfo_7";
createNode place2dTexture -n "place2dTexture_13";
createNode place2dTexture -n "place2dTexture_14";
createNode lambert -n "lambert1_1";
	setAttr ".dc" 1.000000;
	setAttr ".c" -type "float3" 0.400000 0.400000 0.400000;
	addAttr -ln "colladaEffectId" -dt "string";
	setAttr .colladaEffectId -type "string" "lambert1-fx";
	addAttr -ln "colladaMaterialId" -dt "string";
	setAttr .colladaMaterialId -type "string" "lambert1";
createNode shadingEngine -n "lambert1_1SG";
createNode materialInfo -n "materialInfo_8";
createNode file -n "file1";
	setAttr ".ftn" -type "string" "C:/Users/Nocturne/Documents/maya/projects/default/scenes/dark_mansion_panneling/dark_mansion_panneling.dds";
	addAttr -ln "colladaId" -dt "string";
	setAttr .colladaId -type "string" "file1";
createNode file -n "file1_1";
	setAttr ".ftn" -type "string" "C:/Users/Nocturne/Documents/maya/projects/default/scenes/dark_mansion_panneling/dark_mansion_panneling.dds";
	addAttr -ln "colladaId" -dt "string";
	setAttr .colladaId -type "string" "file1";
createNode file -n "file1_2";
	setAttr ".ftn" -type "string" "C:/Users/Nocturne/Documents/maya/projects/default/scenes/dark_mansion_panneling/dark_mansion_panneling.dds";
	addAttr -ln "colladaId" -dt "string";
	setAttr .colladaId -type "string" "file1";
createNode file -n "file1_3";
	setAttr ".ftn" -type "string" "C:/Users/Nocturne/Documents/maya/projects/default/scenes/dark_mansion_panneling/dark_mansion_panneling.dds";
	addAttr -ln "colladaId" -dt "string";
	setAttr .colladaId -type "string" "file1";
createNode file -n "file1_4";
	setAttr ".ftn" -type "string" "C:/Users/Nocturne/Documents/maya/projects/default/scenes/dark_mansion_panneling/dark_mansion_panneling.dds";
	addAttr -ln "colladaId" -dt "string";
	setAttr .colladaId -type "string" "file1";
createNode file -n "file1_5";
	setAttr ".ftn" -type "string" "C:/Users/Nocturne/Documents/maya/projects/default/scenes/dark_mansion_panneling/dark_mansion_panneling.dds";
	addAttr -ln "colladaId" -dt "string";
	setAttr .colladaId -type "string" "file1";
createNode file -n "file4";
	setAttr ".ftn" -type "string" "C:/Users/Nocturne/Documents/maya/projects/default/scenes/mansion_wall_wood/mansion_wall_wood.dds";
	addAttr -ln "colladaId" -dt "string";
	setAttr .colladaId -type "string" "file4";
createNode file -n "file4_1";
	setAttr ".ftn" -type "string" "C:/Users/Nocturne/Documents/maya/projects/default/scenes/mansion_wall_wood/mansion_wall_wood.dds";
	addAttr -ln "colladaId" -dt "string";
	setAttr .colladaId -type "string" "file4";
createNode file -n "file4_2";
	setAttr ".ftn" -type "string" "C:/Users/Nocturne/Documents/maya/projects/default/scenes/mansion_wall_wood/mansion_wall_wood.dds";
	addAttr -ln "colladaId" -dt "string";
	setAttr .colladaId -type "string" "file4";
createNode file -n "file4_3";
	setAttr ".ftn" -type "string" "C:/Users/Nocturne/Documents/maya/projects/default/scenes/mansion_wall_wood/mansion_wall_wood.dds";
	addAttr -ln "colladaId" -dt "string";
	setAttr .colladaId -type "string" "file4";
createNode file -n "file4_4";
	setAttr ".ftn" -type "string" "C:/Users/Nocturne/Documents/maya/projects/default/scenes/mansion_wall_wood/mansion_wall_wood.dds";
	addAttr -ln "colladaId" -dt "string";
	setAttr .colladaId -type "string" "file4";
createNode file -n "file4_5";
	setAttr ".ftn" -type "string" "C:/Users/Nocturne/Documents/maya/projects/default/scenes/mansion_wall_wood/mansion_wall_wood.dds";
	addAttr -ln "colladaId" -dt "string";
	setAttr .colladaId -type "string" "file4";
createNode file -n "file4_6";
	setAttr ".ftn" -type "string" "C:/Users/Nocturne/Documents/maya/projects/default/scenes/mansion_wall_wood/mansion_wall_wood.dds";
	addAttr -ln "colladaId" -dt "string";
	setAttr .colladaId -type "string" "file4";
createNode file -n "file4_7";
	setAttr ".ftn" -type "string" "C:/Users/Nocturne/Documents/maya/projects/default/scenes/mansion_wall_wood/mansion_wall_wood.dds";
	addAttr -ln "colladaId" -dt "string";
	setAttr .colladaId -type "string" "file4";
createNode mesh -n "polySurfaceShape16" -p "|polySurface15|polySurface16";
	addAttr -ln "colladaId" -dt "string";
	setAttr .colladaId -type "string" "polySurfaceShape16";
	setAttr ".vt[0:19]" -0.113439 1.566619 0.050000 -0.898185 1.566619 0.050000 -0.898185 0.722890 0.050000 -0.113439 0.722890 0.050000 -0.898185 1.566619 0.100000 -0.113439 1.566619 0.100000 -0.113439 0.722890 0.100000 -0.898185 0.722890 0.100000 -1.000000 0.000000 0.100000 1.000000 0.000000 0.100000 1.000000 1.800000 0.100000 -1.000000 1.800000 0.100000 0.892546 1.566619 0.100000 0.892546 0.722890 0.100000 0.107800 0.722890 0.100000 0.107800 1.566619 0.100000 0.107800 0.722890 0.050000 0.107800 1.566619 0.050000 0.892546 0.722890 0.050000 0.892546 1.566619 0.050000;
	setAttr ".n[0:101]" 0.000000 0.000000 1.000000 0.000000 0.000000 1.000000 0.000000 0.000000 1.000000 0.000000 0.000000 1.000000 0.000000 0.000000 1.000000 0.000000 0.000000 1.000000 0.000000 -1.000000 0.000000 0.000000 -1.000000 0.000000 0.000000 -1.000000 0.000000 0.000000 -1.000000 0.000000 0.000000 -1.000000 0.000000 0.000000 -1.000000 0.000000 -1.000000 0.000000 0.000000 -1.000000 0.000000 0.000000 -1.000000 0.000000 0.000000 -1.000000 0.000000 0.000000 -1.000000 0.000000 0.000000 -1.000000 0.000000 0.000000 0.000000 1.000000 0.000000 0.000000 1.000000 0.000000 0.000000 1.000000 0.000000 0.000000 1.000000 0.000000 0.000000 1.000000 0.000000 0.000000 1.000000 0.000000 1.000000 0.000000 0.000000 1.000000 0.000000 0.000000 1.000000 0.000000 0.000000 1.000000 0.000000 0.000000 1.000000 0.000000 0.000000 1.000000 0.000000 0.000000 0.000000 0.000000 1.000000 0.000000 0.000000 1.000000 0.000000 0.000000 1.000000 0.000000 0.000000 1.000000 0.000000 0.000000 1.000000 0.000000 0.000000 1.000000 0.000000 0.000000 1.000000 0.000000 0.000000 1.000000 0.000000 0.000000 1.000000 0.000000 0.000000 1.000000 0.000000 0.000000 1.000000 0.000000 0.000000 1.000000 0.000000 0.000000 1.000000 0.000000 0.000000 1.000000 0.000000 0.000000 1.000000 0.000000 0.000000 1.000000 0.000000 0.000000 1.000000 0.000000 0.000000 1.000000 0.000000 0.000000 1.000000 0.000000 0.000000 1.000000 0.000000 0.000000 1.000000 0.000000 0.000000 1.000000 0.000000 0.000000 1.000000 0.000000 0.000000 1.000000 0.000000 0.000000 1.000000 0.000000 0.000000 1.000000 0.000000 0.000000 1.000000 0.000000 0.000000 1.000000 0.000000 0.000000 1.000000 0.000000 0.000000 1.000000 0.000000 0.000000 1.000000 0.000000 0.000000 1.000000 0.000000 0.000000 1.000000 0.000000 0.000000 1.000000 0.000000 0.000000 1.000000 0.000000 0.000000 1.000000 0.000000 0.000000 1.000000 0.000000 0.000000 1.000000 0.000000 0.000000 1.000000 0.000000 0.000000 1.000000 0.000000 0.000000 1.000000 0.000000 0.000000 1.000000 1.000000 0.000000 0.000000 1.000000 0.000000 0.000000 1.000000 0.000000 0.000000 1.000000 0.000000 0.000000 1.000000 0.000000 0.000000 1.000000 0.000000 0.000000 0.000000 1.000000 0.000000 0.000000 1.000000 0.000000 0.000000 1.000000 0.000000 0.000000 1.000000 0.000000 0.000000 1.000000 0.000000 0.000000 1.000000 0.000000 -1.000000 0.000000 0.000000 -1.000000 0.000000 0.000000 -1.000000 0.000000 0.000000 -1.000000 0.000000 0.000000 -1.000000 0.000000 0.000000 -1.000000 0.000000 0.000000 0.000000 -1.000000 0.000000 0.000000 -1.000000 0.000000 0.000000 -1.000000 0.000000 0.000000 -1.000000 0.000000 0.000000 -1.000000 0.000000 0.000000 -1.000000 0.000000 0.000000 0.000000 1.000000 0.000000 0.000000 1.000000 0.000000 0.000000 1.000000 0.000000 0.000000 1.000000 0.000000 0.000000 1.000000 0.000000 0.000000 1.000000;
	setAttr ".usz" 1.000000;
	setAttr ".uvst[0].uvsn" -type "string" "polySurfaceShape16-map1";
	setAttr ".cuvs" -type "string" "polySurfaceShape16-map1";
	setAttr ".uvst[0].uvsp[0:51]" 0.212858 0.353066 0.031966 0.353066 0.212858 0.158578 0.031966 0.158578 0.029435 0.452092 0.029435 0.419964 0.788363 0.452092 0.788364 0.419964 0.070628 0.452573 0.070628 0.419484 0.694219 0.453572 0.694219 0.418485 0.016712 0.453572 0.016712 0.418485 0.775887 0.453234 0.775887 0.418822 0.110979 0.453572 0.110979 0.418485 0.734570 0.452573 0.734570 0.419484 0.000000 0.000000 0.025455 0.160860 0.000000 0.399718 0.025455 0.347878 0.499999 0.399723 0.221641 0.347877 0.473137 0.347877 0.499999 0.000190 0.473137 0.160819 0.221641 0.160820 0.276950 0.160849 0.276951 0.347877 0.110979 0.453572 0.110979 0.418485 0.734570 0.452573 0.734570 0.419484 0.016712 0.453572 0.016712 0.418485 0.775887 0.453234 0.775887 0.418822 0.070628 0.452573 0.070628 0.419484 0.694219 0.453572 0.694219 0.418485 0.029435 0.452092 0.029435 0.419964 0.788363 0.452092 0.788363 0.419964 0.466776 0.353066 0.285884 0.353066 0.466776 0.158578 0.285884 0.158578;
	setAttr ".ed[0:52]" 0 1 0 1 3 0 0 3 0 1 2 0 2 3 0 1 4 0 1 5 0 4 5 0 0 5 0 0 6 0 5 6 0 3 6 0 3 7 0 6 7 0 2 7 0 2 4 0 4 7 0 7 8 0 7 11 0 8 11 0 4 11 0 4 10 0 10 11 0 5 10 0 10 12 0 9 12 0 9 10 0 12 13 0 9 13 0 8 9 0 7 9 0 6 9 0 13 14 0 9 14 0 6 14 0 5 14 0 14 15 0 5 15 0 10 15 0 12 15 0 14 16 0 15 16 0 16 17 0 15 17 0 13 18 0 14 18 0 16 18 0 12 19 0 13 19 0 18 19 0 12 17 0 17 19 0 17 18 0;
	setAttr ".fc[0:33]" -type "polyFaces"  
		f 3 0 1 -3
		mu 0 3 0 1 2 
		f 3 -2 3 4
		mu 0 3 2 1 3 
		f 3 -6 6 -8
		mu 0 3 4 5 6 
		f 3 -7 -1 8
		mu 0 3 6 5 7 
		f 3 -9 9 -11
		mu 0 3 8 9 10 
		f 3 -10 2 11
		mu 0 3 10 9 11 
		f 3 -12 12 -14
		mu 0 3 12 13 14 
		f 3 -13 -5 14
		mu 0 3 14 13 15 
		f 3 -15 15 16
		mu 0 3 16 17 18 
		f 3 -16 -4 5
		mu 0 3 18 17 19 
		f 3 -18 18 -20
		mu 0 3 20 21 22 
		f 3 -17 20 -19
		mu 0 3 21 23 22 
		f 3 -21 21 22
		mu 0 3 22 23 24 
		f 3 7 23 -22
		mu 0 3 23 25 24 
		f 3 24 -26 26
		mu 0 3 24 26 27 
		f 3 27 -29 25
		mu 0 3 26 28 27 
		f 3 29 -31 17
		mu 0 3 20 27 21 
		f 3 30 -32 13
		mu 0 3 21 27 29 
		f 3 32 -34 28
		mu 0 3 28 30 27 
		f 3 33 -35 31
		mu 0 3 27 30 29 
		f 3 34 -36 10
		mu 0 3 29 30 25 
		f 3 36 -38 35
		mu 0 3 30 31 25 
		f 3 37 -39 -24
		mu 0 3 25 31 24 
		f 3 -40 -25 38
		mu 0 3 31 26 24 
		f 3 40 -42 -37
		mu 0 3 32 33 34 
		f 3 41 42 -44
		mu 0 3 34 33 35 
		f 3 44 -46 -33
		mu 0 3 36 37 38 
		f 3 45 -47 -41
		mu 0 3 38 37 39 
		f 3 47 -49 -28
		mu 0 3 40 41 42 
		f 3 48 -50 -45
		mu 0 3 42 41 43 
		f 3 43 -51 39
		mu 0 3 44 45 46 
		f 3 50 51 -48
		mu 0 3 46 45 47 
		f 3 -52 52 49
		mu 0 3 48 49 50 
		f 3 -53 -43 46
		mu 0 3 50 49 51;
createNode mesh -n "polySurfaceShape17" -p "|polySurface15|polySurface17";
	addAttr -ln "colladaId" -dt "string";
	setAttr .colladaId -type "string" "polySurfaceShape17";
	setAttr ".vt[0:3]" -1.000000 1.800000 0.100000 1.000000 1.800000 0.100000 -1.000000 1.872108 0.004247 1.000000 1.872108 0.004247;
	setAttr ".n[0:5]" 0.000000 0.798822 0.601568 0.000000 0.798822 0.601568 0.000000 0.798822 0.601568 0.000000 0.798822 0.601568 0.000000 0.798822 0.601568 0.000000 0.798822 0.601568;
	setAttr ".usz" 1.000000;
	setAttr ".uvst[0].uvsn" -type "string" "polySurfaceShape17-map1";
	setAttr ".cuvs" -type "string" "polySurfaceShape17-map1";
	setAttr ".uvst[0].uvsp[0:3]" 0.000000 0.972160 0.500000 0.972160 0.000000 1.000000 0.500000 1.000000;
	setAttr ".ed[0:4]" 0 1 0 1 2 0 0 2 0 1 3 0 2 3 0;
	setAttr ".fc[0:1]" -type "polyFaces"  
		f 3 0 1 -3
		mu 0 3 0 1 2 
		f 3 -2 3 -5
		mu 0 3 2 1 3;
createNode mesh -n "polySurfaceShape18" -p "|polySurface15|polySurface18";
	addAttr -ln "colladaId" -dt "string";
	setAttr .colladaId -type "string" "polySurfaceShape18";
	setAttr ".vt[0:15]" -1.000000 1.820894 0.000000 1.000000 1.820894 0.000000 1.000000 4.000000 0.200000 -1.000000 4.000000 0.200000 0.553034 2.043624 0.000000 -0.590704 2.043624 0.000000 -0.590704 3.481682 0.000000 0.553034 3.481682 0.000000 -0.590704 3.481682 -0.050022 0.553034 3.481682 -0.050022 -0.590704 2.043624 -0.050022 0.553034 2.043624 -0.050022 -1.000000 3.800000 0.000000 -0.825597 3.800000 0.000000 0.815285 3.800000 0.000000 1.000000 3.800000 0.000000;
	setAttr ".n[0:71]" 0.000000 0.000000 1.000000 0.000000 0.000000 1.000000 0.000000 0.000000 1.000000 0.000000 0.000000 1.000000 0.000000 0.000000 1.000000 0.000000 0.000000 1.000000 0.000000 -0.707107 0.707106 0.000000 -0.707108 0.707106 0.000000 -0.707107 0.707107 0.000000 -0.707107 0.707107 0.000000 -0.707107 0.707106 0.000000 -0.707107 0.707107 0.000000 -0.707107 0.707107 0.000000 -0.707107 0.707106 0.000000 -0.707107 0.707107 0.000000 -0.707106 0.707107 0.000000 -0.707107 0.707107 0.000000 -0.707107 0.707107 0.000000 -1.000000 0.000000 0.000000 -1.000000 0.000000 0.000000 -1.000000 0.000000 0.000000 -1.000000 0.000000 0.000000 -1.000000 0.000000 0.000000 -1.000000 0.000000 1.000000 0.000000 0.000000 1.000000 0.000000 0.000000 1.000000 0.000000 0.000000 1.000000 0.000000 0.000000 1.000000 0.000000 0.000000 1.000000 0.000000 0.000000 0.000000 1.000000 0.000000 0.000000 1.000000 0.000000 0.000000 1.000000 0.000000 0.000000 1.000000 0.000000 0.000000 1.000000 0.000000 0.000000 1.000000 0.000000 -1.000000 0.000000 0.000000 -1.000000 0.000000 0.000000 -1.000000 0.000000 0.000000 -1.000000 0.000000 0.000000 -1.000000 0.000000 0.000000 -1.000000 0.000000 0.000000 0.000000 0.000000 1.000000 0.000000 0.000000 1.000000 0.000000 0.000000 1.000000 0.000000 0.000000 1.000000 0.000000 0.000000 1.000000 0.000000 0.000000 1.000000 0.000000 0.000000 1.000000 0.000000 0.000000 1.000000 0.000000 0.000000 1.000000 0.000000 0.000000 1.000000 0.000000 0.000000 1.000000 0.000000 0.000000 1.000000 0.000000 0.000000 1.000000 0.000000 0.000000 1.000000 0.000000 0.000000 1.000000 0.000000 0.000000 1.000000 0.000000 0.000000 1.000000 0.000000 0.000000 1.000000 0.000000 0.000000 1.000000 0.000000 0.000000 1.000000 0.000000 0.000000 1.000000 0.000000 0.000000 1.000000 0.000000 0.000000 1.000000 0.000000 0.000000 1.000000 0.000000 0.000000 1.000000 0.000000 0.000000 1.000000 0.000000 0.000000 1.000000 0.000000 0.000000 1.000000 0.000000 0.000000 1.000000 0.000000 0.000000 1.000000;
	setAttr ".usz" 1.000000;
	setAttr ".uvst[0].uvsn" -type "string" "polySurfaceShape18-map1";
	setAttr ".cuvs" -type "string" "polySurfaceShape18-map1";
	setAttr ".uvst[0].uvsp[0:31]" 0.102376 0.511012 0.000000 0.455341 0.388268 0.511012 0.500004 0.455345 0.000000 1.000000 0.000000 0.944621 0.043602 0.944797 0.499987 0.999996 0.453806 0.946459 0.499988 0.946645 0.389307 0.887223 0.389307 0.502837 0.402454 0.887223 0.402454 0.502837 0.389307 0.887223 0.389307 0.502837 0.402454 0.887223 0.402454 0.502837 0.389307 0.887223 0.389307 0.502837 0.402454 0.887223 0.402454 0.502837 0.389307 0.887223 0.389307 0.502837 0.402454 0.887223 0.402454 0.502837 0.402709 0.887466 0.402709 0.498219 0.084488 0.887466 0.084488 0.498219 0.102327 0.870449 0.388241 0.870449;
	setAttr ".ed[0:38]" 0 5 0 0 4 0 4 5 0 0 1 0 1 4 0 3 12 0 12 13 0 3 13 0 2 3 0 3 14 0 2 14 0 13 14 0 2 15 0 14 15 0 6 7 0 6 9 0 7 9 0 6 8 0 8 9 0 5 6 0 5 8 0 5 10 0 8 10 0 4 10 0 4 11 0 10 11 0 4 7 0 7 11 0 9 11 0 9 10 0 5 12 0 0 12 0 5 13 0 6 13 0 7 13 0 7 14 0 1 15 0 4 15 0 4 14 0;
	setAttr ".fc[0:23]" -type "polyFaces"  
		f 3 -1 1 2
		mu 0 3 0 1 2 
		f 3 -2 3 4
		mu 0 3 2 1 3 
		f 3 5 6 -8
		mu 0 3 4 5 6 
		f 3 8 9 -11
		mu 0 3 7 4 8 
		f 3 -10 7 11
		mu 0 3 8 4 6 
		f 3 -13 10 13
		mu 0 3 9 7 8 
		f 3 -15 15 -17
		mu 0 3 10 11 12 
		f 3 -16 17 18
		mu 0 3 12 11 13 
		f 3 -20 20 -18
		mu 0 3 14 15 16 
		f 3 -21 21 -23
		mu 0 3 16 15 17 
		f 3 -3 23 -22
		mu 0 3 18 19 20 
		f 3 -24 24 -26
		mu 0 3 20 19 21 
		f 3 26 27 -25
		mu 0 3 22 23 24 
		f 3 -28 16 28
		mu 0 3 24 23 25 
		f 3 -29 29 25
		mu 0 3 26 27 28 
		f 3 -30 -19 22
		mu 0 3 28 27 29 
		f 3 0 30 -32
		mu 0 3 1 0 5 
		f 3 -31 32 -7
		mu 0 3 5 0 6 
		f 3 19 33 -33
		mu 0 3 0 30 6 
		f 3 -12 -35 35
		mu 0 3 8 6 31 
		f 3 34 -34 14
		mu 0 3 31 6 30 
		f 3 36 -38 -5
		mu 0 3 3 9 2 
		f 3 -14 -39 37
		mu 0 3 9 8 2 
		f 3 -36 -27 38
		mu 0 3 8 31 2;
connectAttr "lambert1_1SG.msg" "materialInfo_8.sg";
connectAttr "lambert1_1SG.pa" ":renderPartition.st" -na;
connectAttr "lambert1_1.oc" "lambert1_1SG.ss";
connectAttr "lambert1_1.msg" "materialInfo_8.m";
connectAttr "blinn1SG.msg" "materialInfo_1.sg";
connectAttr "blinn1SG.pa" ":renderPartition.st" -na;
connectAttr "blinn1.oc" "blinn1SG.ss";
connectAttr "blinn1.msg" "materialInfo_1.m";
connectAttr "blinn2SG.msg" "materialInfo_2.sg";
connectAttr "blinn2SG.pa" ":renderPartition.st" -na;
connectAttr "blinn2.oc" "blinn2SG.ss";
connectAttr "blinn2.msg" "materialInfo_2.m";
connectAttr "blinn3SG.msg" "materialInfo_3.sg";
connectAttr "blinn3SG.pa" ":renderPartition.st" -na;
connectAttr "blinn3.oc" "blinn3SG.ss";
connectAttr "blinn3.msg" "materialInfo_3.m";
connectAttr "blinn4SG.msg" "materialInfo_4.sg";
connectAttr "blinn4SG.pa" ":renderPartition.st" -na;
connectAttr "blinn4.oc" "blinn4SG.ss";
connectAttr "blinn4.msg" "materialInfo_4.m";
connectAttr "blinn5SG.msg" "materialInfo_5.sg";
connectAttr "blinn5SG.pa" ":renderPartition.st" -na;
connectAttr "blinn5.oc" "blinn5SG.ss";
connectAttr "blinn5.msg" "materialInfo_5.m";
connectAttr "blinn6SG.msg" "materialInfo_6.sg";
connectAttr "blinn6SG.pa" ":renderPartition.st" -na;
connectAttr "blinn6.oc" "blinn6SG.ss";
connectAttr "blinn6.msg" "materialInfo_6.m";
connectAttr "blinn7SG.msg" "materialInfo_7.sg";
connectAttr "blinn7SG.pa" ":renderPartition.st" -na;
connectAttr "blinn7.oc" "blinn7SG.ss";
connectAttr "blinn7.msg" "materialInfo_7.m";
connectAttr "|polySurface15|polySurface16|polySurfaceShape16.iog" "blinn4SG.dsm" -na;
connectAttr "|polySurface15|polySurface17|polySurfaceShape17.iog" "blinn7SG.dsm" -na;
connectAttr "|polySurface15|polySurface18|polySurfaceShape18.iog" "blinn6SG.dsm" -na;
connectAttr "lambert1_1.msg" ":defaultShaderList1.s" -na;
connectAttr "blinn1.msg" ":defaultShaderList1.s" -na;
connectAttr "blinn2.msg" ":defaultShaderList1.s" -na;
connectAttr "blinn3.msg" ":defaultShaderList1.s" -na;
connectAttr "blinn4.msg" ":defaultShaderList1.s" -na;
connectAttr "blinn5.msg" ":defaultShaderList1.s" -na;
connectAttr "blinn6.msg" ":defaultShaderList1.s" -na;
connectAttr "blinn7.msg" ":defaultShaderList1.s" -na;
connectAttr "defaultLightSet.msg" "lightLinker1.lnk[0].llnk";
connectAttr "lambert1_1SG.msg" "lightLinker1.lnk[0].olnk";
connectAttr "defaultLightSet.msg" "lightLinker1.slnk[0].sllk";
connectAttr "lambert1_1SG.msg" "lightLinker1.slnk[0].solk";
connectAttr "defaultLightSet.msg" "lightLinker1.lnk[1].llnk";
connectAttr "blinn1SG.msg" "lightLinker1.lnk[1].olnk";
connectAttr "defaultLightSet.msg" "lightLinker1.slnk[1].sllk";
connectAttr "blinn1SG.msg" "lightLinker1.slnk[1].solk";
connectAttr "defaultLightSet.msg" "lightLinker1.lnk[2].llnk";
connectAttr "blinn2SG.msg" "lightLinker1.lnk[2].olnk";
connectAttr "defaultLightSet.msg" "lightLinker1.slnk[2].sllk";
connectAttr "blinn2SG.msg" "lightLinker1.slnk[2].solk";
connectAttr "defaultLightSet.msg" "lightLinker1.lnk[3].llnk";
connectAttr "blinn3SG.msg" "lightLinker1.lnk[3].olnk";
connectAttr "defaultLightSet.msg" "lightLinker1.slnk[3].sllk";
connectAttr "blinn3SG.msg" "lightLinker1.slnk[3].solk";
connectAttr "defaultLightSet.msg" "lightLinker1.lnk[4].llnk";
connectAttr "blinn4SG.msg" "lightLinker1.lnk[4].olnk";
connectAttr "defaultLightSet.msg" "lightLinker1.slnk[4].sllk";
connectAttr "blinn4SG.msg" "lightLinker1.slnk[4].solk";
connectAttr "defaultLightSet.msg" "lightLinker1.lnk[5].llnk";
connectAttr "blinn5SG.msg" "lightLinker1.lnk[5].olnk";
connectAttr "defaultLightSet.msg" "lightLinker1.slnk[5].sllk";
connectAttr "blinn5SG.msg" "lightLinker1.slnk[5].solk";
connectAttr "defaultLightSet.msg" "lightLinker1.lnk[6].llnk";
connectAttr "blinn6SG.msg" "lightLinker1.lnk[6].olnk";
connectAttr "defaultLightSet.msg" "lightLinker1.slnk[6].sllk";
connectAttr "blinn6SG.msg" "lightLinker1.slnk[6].solk";
connectAttr "defaultLightSet.msg" "lightLinker1.lnk[7].llnk";
connectAttr "blinn7SG.msg" "lightLinker1.lnk[7].olnk";
connectAttr "defaultLightSet.msg" "lightLinker1.slnk[7].sllk";
connectAttr "blinn7SG.msg" "lightLinker1.slnk[7].solk";
connectAttr "defaultLightSet.msg" "lightLinker1.lnk[8].llnk";
connectAttr ":initialShadingGroup.msg" "lightLinker1.lnk[8].olnk";
connectAttr "defaultLightSet.msg" "lightLinker1.slnk[8].sllk";
connectAttr ":initialShadingGroup.msg" "lightLinker1.slnk[8].solk";
connectAttr "lightLinker1.msg" ":lightList1.ln[0]";
connectAttr "place2dTexture_1.o" "file1.uv";
connectAttr "place2dTexture_1.ofs" "file1.fs";
connectAttr "place2dTexture_1.vt1" "file1.vt1";
connectAttr "place2dTexture_1.vt2" "file1.vt2";
connectAttr "place2dTexture_1.vt3" "file1.vt3";
connectAttr "place2dTexture_1.vc1" "file1.vc1";
connectAttr "place2dTexture_1.of" "file1.of";
connectAttr "place2dTexture_1.s" "file1.s";
connectAttr "place2dTexture_1.c" "file1.c";
connectAttr "place2dTexture_1.tf" "file1.tf";
connectAttr "place2dTexture_1.mu" "file1.mu";
connectAttr "place2dTexture_1.mv" "file1.mv";
connectAttr "place2dTexture_1.wu" "file1.wu";
connectAttr "place2dTexture_1.wv" "file1.wv";
connectAttr "place2dTexture_1.n" "file1.n";
connectAttr "place2dTexture_1.r" "file1.ro";
connectAttr "place2dTexture_1.re" "file1.re";
connectAttr "place2dTexture_2.o" "file1_1.uv";
connectAttr "place2dTexture_2.ofs" "file1_1.fs";
connectAttr "place2dTexture_2.vt1" "file1_1.vt1";
connectAttr "place2dTexture_2.vt2" "file1_1.vt2";
connectAttr "place2dTexture_2.vt3" "file1_1.vt3";
connectAttr "place2dTexture_2.vc1" "file1_1.vc1";
connectAttr "place2dTexture_2.of" "file1_1.of";
connectAttr "place2dTexture_2.s" "file1_1.s";
connectAttr "place2dTexture_2.c" "file1_1.c";
connectAttr "place2dTexture_2.tf" "file1_1.tf";
connectAttr "place2dTexture_2.mu" "file1_1.mu";
connectAttr "place2dTexture_2.mv" "file1_1.mv";
connectAttr "place2dTexture_2.wu" "file1_1.wu";
connectAttr "place2dTexture_2.wv" "file1_1.wv";
connectAttr "place2dTexture_2.n" "file1_1.n";
connectAttr "place2dTexture_2.r" "file1_1.ro";
connectAttr "place2dTexture_2.re" "file1_1.re";
connectAttr "place2dTexture_3.o" "file1_2.uv";
connectAttr "place2dTexture_3.ofs" "file1_2.fs";
connectAttr "place2dTexture_3.vt1" "file1_2.vt1";
connectAttr "place2dTexture_3.vt2" "file1_2.vt2";
connectAttr "place2dTexture_3.vt3" "file1_2.vt3";
connectAttr "place2dTexture_3.vc1" "file1_2.vc1";
connectAttr "place2dTexture_3.of" "file1_2.of";
connectAttr "place2dTexture_3.s" "file1_2.s";
connectAttr "place2dTexture_3.c" "file1_2.c";
connectAttr "place2dTexture_3.tf" "file1_2.tf";
connectAttr "place2dTexture_3.mu" "file1_2.mu";
connectAttr "place2dTexture_3.mv" "file1_2.mv";
connectAttr "place2dTexture_3.wu" "file1_2.wu";
connectAttr "place2dTexture_3.wv" "file1_2.wv";
connectAttr "place2dTexture_3.n" "file1_2.n";
connectAttr "place2dTexture_3.r" "file1_2.ro";
connectAttr "place2dTexture_3.re" "file1_2.re";
connectAttr "place2dTexture_4.o" "file1_3.uv";
connectAttr "place2dTexture_4.ofs" "file1_3.fs";
connectAttr "place2dTexture_4.vt1" "file1_3.vt1";
connectAttr "place2dTexture_4.vt2" "file1_3.vt2";
connectAttr "place2dTexture_4.vt3" "file1_3.vt3";
connectAttr "place2dTexture_4.vc1" "file1_3.vc1";
connectAttr "place2dTexture_4.of" "file1_3.of";
connectAttr "place2dTexture_4.s" "file1_3.s";
connectAttr "place2dTexture_4.c" "file1_3.c";
connectAttr "place2dTexture_4.tf" "file1_3.tf";
connectAttr "place2dTexture_4.mu" "file1_3.mu";
connectAttr "place2dTexture_4.mv" "file1_3.mv";
connectAttr "place2dTexture_4.wu" "file1_3.wu";
connectAttr "place2dTexture_4.wv" "file1_3.wv";
connectAttr "place2dTexture_4.n" "file1_3.n";
connectAttr "place2dTexture_4.r" "file1_3.ro";
connectAttr "place2dTexture_4.re" "file1_3.re";
connectAttr "place2dTexture_5.o" "file1_4.uv";
connectAttr "place2dTexture_5.ofs" "file1_4.fs";
connectAttr "place2dTexture_5.vt1" "file1_4.vt1";
connectAttr "place2dTexture_5.vt2" "file1_4.vt2";
connectAttr "place2dTexture_5.vt3" "file1_4.vt3";
connectAttr "place2dTexture_5.vc1" "file1_4.vc1";
connectAttr "place2dTexture_5.of" "file1_4.of";
connectAttr "place2dTexture_5.s" "file1_4.s";
connectAttr "place2dTexture_5.c" "file1_4.c";
connectAttr "place2dTexture_5.tf" "file1_4.tf";
connectAttr "place2dTexture_5.mu" "file1_4.mu";
connectAttr "place2dTexture_5.mv" "file1_4.mv";
connectAttr "place2dTexture_5.wu" "file1_4.wu";
connectAttr "place2dTexture_5.wv" "file1_4.wv";
connectAttr "place2dTexture_5.n" "file1_4.n";
connectAttr "place2dTexture_5.r" "file1_4.ro";
connectAttr "place2dTexture_5.re" "file1_4.re";
connectAttr "place2dTexture_6.o" "file1_5.uv";
connectAttr "place2dTexture_6.ofs" "file1_5.fs";
connectAttr "place2dTexture_6.vt1" "file1_5.vt1";
connectAttr "place2dTexture_6.vt2" "file1_5.vt2";
connectAttr "place2dTexture_6.vt3" "file1_5.vt3";
connectAttr "place2dTexture_6.vc1" "file1_5.vc1";
connectAttr "place2dTexture_6.of" "file1_5.of";
connectAttr "place2dTexture_6.s" "file1_5.s";
connectAttr "place2dTexture_6.c" "file1_5.c";
connectAttr "place2dTexture_6.tf" "file1_5.tf";
connectAttr "place2dTexture_6.mu" "file1_5.mu";
connectAttr "place2dTexture_6.mv" "file1_5.mv";
connectAttr "place2dTexture_6.wu" "file1_5.wu";
connectAttr "place2dTexture_6.wv" "file1_5.wv";
connectAttr "place2dTexture_6.n" "file1_5.n";
connectAttr "place2dTexture_6.r" "file1_5.ro";
connectAttr "place2dTexture_6.re" "file1_5.re";
connectAttr "place2dTexture_7.o" "file4.uv";
connectAttr "place2dTexture_7.ofs" "file4.fs";
connectAttr "place2dTexture_7.vt1" "file4.vt1";
connectAttr "place2dTexture_7.vt2" "file4.vt2";
connectAttr "place2dTexture_7.vt3" "file4.vt3";
connectAttr "place2dTexture_7.vc1" "file4.vc1";
connectAttr "place2dTexture_7.of" "file4.of";
connectAttr "place2dTexture_7.s" "file4.s";
connectAttr "place2dTexture_7.c" "file4.c";
connectAttr "place2dTexture_7.tf" "file4.tf";
connectAttr "place2dTexture_7.mu" "file4.mu";
connectAttr "place2dTexture_7.mv" "file4.mv";
connectAttr "place2dTexture_7.wu" "file4.wu";
connectAttr "place2dTexture_7.wv" "file4.wv";
connectAttr "place2dTexture_7.n" "file4.n";
connectAttr "place2dTexture_7.r" "file4.ro";
connectAttr "place2dTexture_7.re" "file4.re";
connectAttr "place2dTexture_8.o" "file4_1.uv";
connectAttr "place2dTexture_8.ofs" "file4_1.fs";
connectAttr "place2dTexture_8.vt1" "file4_1.vt1";
connectAttr "place2dTexture_8.vt2" "file4_1.vt2";
connectAttr "place2dTexture_8.vt3" "file4_1.vt3";
connectAttr "place2dTexture_8.vc1" "file4_1.vc1";
connectAttr "place2dTexture_8.of" "file4_1.of";
connectAttr "place2dTexture_8.s" "file4_1.s";
connectAttr "place2dTexture_8.c" "file4_1.c";
connectAttr "place2dTexture_8.tf" "file4_1.tf";
connectAttr "place2dTexture_8.mu" "file4_1.mu";
connectAttr "place2dTexture_8.mv" "file4_1.mv";
connectAttr "place2dTexture_8.wu" "file4_1.wu";
connectAttr "place2dTexture_8.wv" "file4_1.wv";
connectAttr "place2dTexture_8.n" "file4_1.n";
connectAttr "place2dTexture_8.r" "file4_1.ro";
connectAttr "place2dTexture_8.re" "file4_1.re";
connectAttr "place2dTexture_9.o" "file4_2.uv";
connectAttr "place2dTexture_9.ofs" "file4_2.fs";
connectAttr "place2dTexture_9.vt1" "file4_2.vt1";
connectAttr "place2dTexture_9.vt2" "file4_2.vt2";
connectAttr "place2dTexture_9.vt3" "file4_2.vt3";
connectAttr "place2dTexture_9.vc1" "file4_2.vc1";
connectAttr "place2dTexture_9.of" "file4_2.of";
connectAttr "place2dTexture_9.s" "file4_2.s";
connectAttr "place2dTexture_9.c" "file4_2.c";
connectAttr "place2dTexture_9.tf" "file4_2.tf";
connectAttr "place2dTexture_9.mu" "file4_2.mu";
connectAttr "place2dTexture_9.mv" "file4_2.mv";
connectAttr "place2dTexture_9.wu" "file4_2.wu";
connectAttr "place2dTexture_9.wv" "file4_2.wv";
connectAttr "place2dTexture_9.n" "file4_2.n";
connectAttr "place2dTexture_9.r" "file4_2.ro";
connectAttr "place2dTexture_9.re" "file4_2.re";
connectAttr "place2dTexture_10.o" "file4_3.uv";
connectAttr "place2dTexture_10.ofs" "file4_3.fs";
connectAttr "place2dTexture_10.vt1" "file4_3.vt1";
connectAttr "place2dTexture_10.vt2" "file4_3.vt2";
connectAttr "place2dTexture_10.vt3" "file4_3.vt3";
connectAttr "place2dTexture_10.vc1" "file4_3.vc1";
connectAttr "place2dTexture_10.of" "file4_3.of";
connectAttr "place2dTexture_10.s" "file4_3.s";
connectAttr "place2dTexture_10.c" "file4_3.c";
connectAttr "place2dTexture_10.tf" "file4_3.tf";
connectAttr "place2dTexture_10.mu" "file4_3.mu";
connectAttr "place2dTexture_10.mv" "file4_3.mv";
connectAttr "place2dTexture_10.wu" "file4_3.wu";
connectAttr "place2dTexture_10.wv" "file4_3.wv";
connectAttr "place2dTexture_10.n" "file4_3.n";
connectAttr "place2dTexture_10.r" "file4_3.ro";
connectAttr "place2dTexture_10.re" "file4_3.re";
connectAttr "place2dTexture_11.o" "file4_4.uv";
connectAttr "place2dTexture_11.ofs" "file4_4.fs";
connectAttr "place2dTexture_11.vt1" "file4_4.vt1";
connectAttr "place2dTexture_11.vt2" "file4_4.vt2";
connectAttr "place2dTexture_11.vt3" "file4_4.vt3";
connectAttr "place2dTexture_11.vc1" "file4_4.vc1";
connectAttr "place2dTexture_11.of" "file4_4.of";
connectAttr "place2dTexture_11.s" "file4_4.s";
connectAttr "place2dTexture_11.c" "file4_4.c";
connectAttr "place2dTexture_11.tf" "file4_4.tf";
connectAttr "place2dTexture_11.mu" "file4_4.mu";
connectAttr "place2dTexture_11.mv" "file4_4.mv";
connectAttr "place2dTexture_11.wu" "file4_4.wu";
connectAttr "place2dTexture_11.wv" "file4_4.wv";
connectAttr "place2dTexture_11.n" "file4_4.n";
connectAttr "place2dTexture_11.r" "file4_4.ro";
connectAttr "place2dTexture_11.re" "file4_4.re";
connectAttr "place2dTexture_12.o" "file4_5.uv";
connectAttr "place2dTexture_12.ofs" "file4_5.fs";
connectAttr "place2dTexture_12.vt1" "file4_5.vt1";
connectAttr "place2dTexture_12.vt2" "file4_5.vt2";
connectAttr "place2dTexture_12.vt3" "file4_5.vt3";
connectAttr "place2dTexture_12.vc1" "file4_5.vc1";
connectAttr "place2dTexture_12.of" "file4_5.of";
connectAttr "place2dTexture_12.s" "file4_5.s";
connectAttr "place2dTexture_12.c" "file4_5.c";
connectAttr "place2dTexture_12.tf" "file4_5.tf";
connectAttr "place2dTexture_12.mu" "file4_5.mu";
connectAttr "place2dTexture_12.mv" "file4_5.mv";
connectAttr "place2dTexture_12.wu" "file4_5.wu";
connectAttr "place2dTexture_12.wv" "file4_5.wv";
connectAttr "place2dTexture_12.n" "file4_5.n";
connectAttr "place2dTexture_12.r" "file4_5.ro";
connectAttr "place2dTexture_12.re" "file4_5.re";
connectAttr "place2dTexture_13.o" "file4_6.uv";
connectAttr "place2dTexture_13.ofs" "file4_6.fs";
connectAttr "place2dTexture_13.vt1" "file4_6.vt1";
connectAttr "place2dTexture_13.vt2" "file4_6.vt2";
connectAttr "place2dTexture_13.vt3" "file4_6.vt3";
connectAttr "place2dTexture_13.vc1" "file4_6.vc1";
connectAttr "place2dTexture_13.of" "file4_6.of";
connectAttr "place2dTexture_13.s" "file4_6.s";
connectAttr "place2dTexture_13.c" "file4_6.c";
connectAttr "place2dTexture_13.tf" "file4_6.tf";
connectAttr "place2dTexture_13.mu" "file4_6.mu";
connectAttr "place2dTexture_13.mv" "file4_6.mv";
connectAttr "place2dTexture_13.wu" "file4_6.wu";
connectAttr "place2dTexture_13.wv" "file4_6.wv";
connectAttr "place2dTexture_13.n" "file4_6.n";
connectAttr "place2dTexture_13.r" "file4_6.ro";
connectAttr "place2dTexture_13.re" "file4_6.re";
connectAttr "place2dTexture_14.o" "file4_7.uv";
connectAttr "place2dTexture_14.ofs" "file4_7.fs";
connectAttr "place2dTexture_14.vt1" "file4_7.vt1";
connectAttr "place2dTexture_14.vt2" "file4_7.vt2";
connectAttr "place2dTexture_14.vt3" "file4_7.vt3";
connectAttr "place2dTexture_14.vc1" "file4_7.vc1";
connectAttr "place2dTexture_14.of" "file4_7.of";
connectAttr "place2dTexture_14.s" "file4_7.s";
connectAttr "place2dTexture_14.c" "file4_7.c";
connectAttr "place2dTexture_14.tf" "file4_7.tf";
connectAttr "place2dTexture_14.mu" "file4_7.mu";
connectAttr "place2dTexture_14.mv" "file4_7.mv";
connectAttr "place2dTexture_14.wu" "file4_7.wu";
connectAttr "place2dTexture_14.wv" "file4_7.wv";
connectAttr "place2dTexture_14.n" "file4_7.n";
connectAttr "place2dTexture_14.r" "file4_7.ro";
connectAttr "place2dTexture_14.re" "file4_7.re";
connectAttr "file1.msg" ":defaultTextureList1.tx" -na;
connectAttr "file1.oc" "blinn1.c";
connectAttr "file1.msg" "materialInfo_1.t" -na;
connectAttr "file1_1.msg" ":defaultTextureList1.tx" -na;
connectAttr "file1_1.ot" "blinn1.it";
connectAttr "file1_1.msg" "materialInfo_1.t" -na;
connectAttr "file1_2.msg" ":defaultTextureList1.tx" -na;
connectAttr "file1_2.oc" "blinn2.c";
connectAttr "file1_2.msg" "materialInfo_2.t" -na;
connectAttr "file1_3.msg" ":defaultTextureList1.tx" -na;
connectAttr "file1_3.ot" "blinn2.it";
connectAttr "file1_3.msg" "materialInfo_2.t" -na;
connectAttr "file1_4.msg" ":defaultTextureList1.tx" -na;
connectAttr "file1_4.oc" "blinn3.c";
connectAttr "file1_4.msg" "materialInfo_3.t" -na;
connectAttr "file1_5.msg" ":defaultTextureList1.tx" -na;
connectAttr "file1_5.ot" "blinn3.it";
connectAttr "file1_5.msg" "materialInfo_3.t" -na;
connectAttr "file4.msg" ":defaultTextureList1.tx" -na;
connectAttr "file4.oc" "blinn4.c";
connectAttr "file4.msg" "materialInfo_4.t" -na;
connectAttr "file4_1.msg" ":defaultTextureList1.tx" -na;
connectAttr "file4_1.ot" "blinn4.it";
connectAttr "file4_1.msg" "materialInfo_4.t" -na;
connectAttr "file4_2.msg" ":defaultTextureList1.tx" -na;
connectAttr "file4_2.oc" "blinn5.c";
connectAttr "file4_2.msg" "materialInfo_5.t" -na;
connectAttr "file4_3.msg" ":defaultTextureList1.tx" -na;
connectAttr "file4_3.ot" "blinn5.it";
connectAttr "file4_3.msg" "materialInfo_5.t" -na;
connectAttr "file4_4.msg" ":defaultTextureList1.tx" -na;
connectAttr "file4_4.oc" "blinn6.c";
connectAttr "file4_4.msg" "materialInfo_6.t" -na;
connectAttr "file4_5.msg" ":defaultTextureList1.tx" -na;
connectAttr "file4_5.ot" "blinn6.it";
connectAttr "file4_5.msg" "materialInfo_6.t" -na;
connectAttr "file4_6.msg" ":defaultTextureList1.tx" -na;
connectAttr "file4_6.oc" "blinn7.c";
connectAttr "file4_6.msg" "materialInfo_7.t" -na;
connectAttr "file4_7.msg" ":defaultTextureList1.tx" -na;
connectAttr "file4_7.ot" "blinn7.it";
connectAttr "file4_7.msg" "materialInfo_7.t" -na;
